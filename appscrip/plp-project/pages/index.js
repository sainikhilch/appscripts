// pages/index.js
import Head from 'next/head';
import ProductCard from '../components/ProductCard'; // A reusable component for products
import styles from '../styles/Home.module.css';

export default function Home({ products }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Product Listing Page</title>
        <meta name="description" content="Discover a variety of products on our store." />
      </Head>
      <h1 className={styles.title}>Our Products</h1>
      <div className={styles.products}>
        {products.map((product) => (
          <ProductCard key={product.id} product={product} />
        ))}
      </div>
    </div>
  );
}

// Fetching data on the server-side (Server-Side Rendering)
export async function getServerSideProps() {
  const products = [
    { id: 1, name: 'Product 1', image: '/images/product1.jpg', description: 'Description 1', price: 29.99 },
    { id: 2, name: 'Product 2', image: '/images/product2.jpg', description: 'Description 2', price: 39.99 },
    { id: 3, name: 'Product 3', image: '/images/product3.jpg', description: 'Description 3', price: 49.99 },
  ];

  return { props: { products } };
}
